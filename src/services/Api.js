import axios from "axios";
export default () => {
  let axiosInstance = axios.create({
    baseURL: ``,
    withCredentials: false,
    headers: {},
  });

  axiosInstance.interceptors.response.use(
    (response) => {
      try {
        return response;
      } catch (e) {
        return response;
      }
    },
    (error) => {
      if (error.response) {
        console.log("Error in API response : " + error.response.data.error);
      }
      return Promise.reject(error);
    }
  );
  return axiosInstance;
};
