import Api from "./Api";
const URL = "http://jsonplaceholder.typicode.com/posts";

export default {
  // Get Posts
  get_posts(paramObj) {
    return Api().get(
      `${URL}?_start=${paramObj.start}&_limit=${paramObj.limit}`
    );
  },
  // Create Post
  create_post(post) {
    return Api().post(`${URL}`, post);
  },
  // Update Post
  update_post(id, post) {
    return Api().put(`${URL}/${id}`, post);
  },
  // Delete Post
  delete_post(id) {
    return Api().delete(`${URL}/${id}${location.search}`);
  },
};
