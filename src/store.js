import Vue from "vue";
import Vuex from "vuex";
import Services from "./services/services";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    start: 0,
    limit: 10,
    posts: [],
  },
  actions: {
    getPosts({ state, commit }, start) {
      const params = { start, limit: state.limit };
      Services.get_posts(params).then((resp) => {
        console.log(resp);
        const posts = resp.data;
        commit("load_posts", posts);
      });
    },
    changePage({ commit }, start) {
      commit("change_page", start);
    },
    createPost({ commit }, postObj) {
      return new Promise((resolve) => {
        Services.create_post(postObj).then((resp) => {
          commit("set_post", resp.data);
          resolve(resp.data);
        });
      }).catch(function(error) {
        console.log(error);
      });
    },
    async updatePost({ commit }, postObj) {
      console.log("updatePost", postObj);
      const { data } = await Services.put_post(postObj);
      commit("set_post", data);
      return data;
    },
    deletePost({ state, commit }, id) {
      return new Promise((resolve) => {
        Services.delete_post(id).then((resp) => {
          const posts = state.posts.filter((post) => post.id !== id);
          commit("update_posts", posts);
          resolve(resp.data);
        });
      }).catch(function(error) {
        console.log(error);
        //reject(error);
      });
    },
  },
  mutations: {
    load_posts(state, payload) {
      state.posts = payload;
    },
    update_posts(state, payload) {
      state.posts = payload;
    },
    change_page(state, payload) {
      state.start = payload;
    },
    set_post(state, payload) {
      state.posts.push(payload);
    },
  },
});
